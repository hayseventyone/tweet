/*jshint esversion: 6 */
console.log('hello Yan');
const form = document.querySelector('form');
const loadingElement = document.querySelector('.loading');
const TweetMeElement = document.querySelector('.TweetMeElement');

const API_URL = window.location.hostname === '127.0.0.1' ? 'http://localhost:5000/TweetMe' : 'https://tweetme.now.sh/tweetme';
//const API_URL ='http://localhost:5000/TweetMe';

loadingElement.style.display = 'none';



listAllTweetMe();

form.addEventListener('submit', (event) => {
	event.preventDefault();
	const formData = new FormData(form);
	const name = formData.get('name');
	const content = formData.get('content');
	const dataSend = {
		name,
		content

	};
	console.log(dataSend);
	form.style.display = 'none';
	loadingElement.style.display = '';

	fetch(API_URL, {
			method: 'POST',
			body: JSON.stringify(dataSend),
			headers: {
				'content-type': 'application/json'
			}
		})
		.then(Response => Response.json())
		.then(createdTweetMe => {
			form.reset();
			setTimeout(() => {
				form.style.display = '';	
			}, 15000);
			
            listAllTweetMe();
		});
});


function calDatetimeDiff(CreatedDate){
	
	let diff = Math.abs(new Date() - CreatedDate);
	
	let years = Math.floor(diff / (365*60*60*24));
	let months = Math.floor((diff - years * 365*60*60*24) / (30*60*60*24));
	let days = Math.floor((diff - years * 365*60*60*24 - months*30*60*60*24)/ (60*60*24));

	return months;//, days// ("${years}  ${months} ${days}");  
      
}


function listAllTweetMe() {
	TweetMeElement.innerHTML ='';
	fetch(API_URL)
		.then(Response => Response.json())
		.then(TweetMe => {
			
			TweetMe.reverse();
			TweetMe.forEach(tweet => {
				const div = document.createElement('div');

				const header = document.createElement('h4');
				header.textContent = tweet.name;
				const contents = document.createElement('p');
				
				contents.textContent = tweet.content;
				const date = document.createElement('small');
				date.style.color = "#999";
				date.textContent =	calDatetimeDiff(new Date(tweet.created));
				// new Date(tweet.created) ;
			

				div.appendChild(header);
				div.appendChild(contents);
				div.appendChild(date);
				
				TweetMeElement.appendChild(div);
	




			});
			loadingElement.style.display = 'none';
		});


}