/*jshint esversion: 6 */
const express = require('express');
const cors = require('cors');
const monk = require('monk');
const Filter = require('bad-words');
const rateLimit = require('express-rate-limit');
const app = express();


const db = monk(process.env.MONGO_URI || 'localhost/NewTwiterMe');
const TweetMe = db.get('TweetMe');

filter = new Filter();

app.use(cors());
app.use(express.json());

app.get('/', (req, res) => {
	res.json({
		message: 'Yeb Pa &#X1F357'
	});
});


app.get('/tweetMe', (req, res) => {
	TweetMe
		.find()
		.then(TweetMe => {
			res.json(TweetMe);
		});
});

function isValidateData(SendData) {
	return SendData.name && SendData.name.toString().trim() !== '' &&
		SendData.content && SendData.content.toString().trim() !== '';
}
app.use(rateLimit({
	windowMs: 10 * 1000,
	max: 1

}));

app.post('/TweetMe', (req, res) => {


	if (isValidateData(req.body)) {

		const SendData = {
			name: filter.clean(req.body.name.toString()),
			content: filter.clean(req.body.content.toString()),
			created: new Date()

		};

		TweetMe
			.insert(SendData)
			.then(createdTweetMe => {
				res.json(createdTweetMe);
				console.log(createdTweetMe);
			});

	} else {
		res.status(442);
		res.json({
			message: 'Name or Content are required!'
		});
	}
});
app.listen(5000, () => {
	console.log('Listening on http://localhost:5000');
});